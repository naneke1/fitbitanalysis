﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using FitbitAnalysis.Model;
using FitbitAnalysis.Processor;
using FitbitAnalysis.Report;

namespace FitbitAnalysis.View
{
    /// <summary>
    ///     An main page that will allow the user to load a CSV file of Fitbit data.
    /// </summary>
    public sealed partial class MainPage
    {
        #region Data members

        /// <summary>
        ///     The application height
        /// </summary>
        public const int ApplicationHeight = 600;

        /// <summary>
        ///     The application width
        /// </summary>
        public const int ApplicationWidth = 1200;

        private readonly FitbitDataLibrary dataLibrary;

        private FitbitDataReport dataReport;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="MainPage" /> class.
        /// </summary>
        public MainPage()
        {
            this.InitializeComponent();

            ApplicationView.PreferredLaunchViewSize = new Size {Width = ApplicationWidth, Height = ApplicationHeight};
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;
            ApplicationView.GetForCurrentView().SetPreferredMinSize(new Size(ApplicationWidth, ApplicationHeight));

            this.dataLibrary = new FitbitDataLibrary();
            this.dataReport = new FitbitDataReport(this.dataLibrary);
        }

        #endregion

        #region Methods

        private async void loadButton_Click(object sender, RoutedEventArgs e)
        {
            this.dataLibrary.Clear();

            var file = await openFilePickerAndSelectFile();

            if (file == null)
            {
                this.outputTextBox.Text = "File could not be loaded";
            }

            if (this.outputTextBox.Text != string.Empty)
            {
                var processor = new DataProcessor {Library = this.dataLibrary};
                await processor.Process(file);

                await this.displayUserPromptDialog();
            }
            else
            {
                var processor = new DataProcessor {Library = this.dataLibrary};
                await processor.Process(file);
                this.outputTextBox.Text = this.dataReport.DisplayFitbitDataReport();
            }
        }

        private async Task displayUserPromptDialog()
        {
            var userpromptDialog = new ContentDialog {
                Title = "Merge or Replace",
                Content = "Would you like to merge new file into existing data or replace exisiting data?",
                PrimaryButtonText = "Merge",
                SecondaryButtonText = "Replace"
            };

            var promptResult = await userpromptDialog.ShowAsync();
            switch (promptResult)
            {
                case ContentDialogResult.Primary:
                    //TODO Add merge functionality   
                    break;
                case ContentDialogResult.Secondary:
                    this.reloadDataReport();
                    break;
            }
        }

        private static async Task<StorageFile> openFilePickerAndSelectFile()
        {
            var file = await new FileOpenPicker {
                ViewMode = PickerViewMode.Thumbnail,
                SuggestedStartLocation = PickerLocationId.DocumentsLibrary,
                FileTypeFilter = {".csv", ".txt"}
            }.PickSingleFileAsync();
            return file;
        }

        private void stepsMoreThan_Button_OnClick(object sender, RoutedEventArgs e)
        {
            var steps = int.Parse(this.stepsMoreThanTextBox.Text);
            this.dataReport.DaysWithStepsMoreThan(steps);
            this.reloadDataReport();
        }

        private void reloadDataReport()
        {
            this.dataReport = new FitbitDataReport(this.dataLibrary);
            this.outputTextBox.Text = this.dataReport.DisplayFitbitDataReport();
        }

        private void stepsBetweenButton_OnClick(object sender, RoutedEventArgs e)
        {
            var steps = int.Parse(this.stepsTextBox.Text);
            var category = int.Parse(this.categoryTextBox.Text);
            this.dataReport.DaysWithStepsBetween(steps, category);
            this.reloadDataReport();
        }

        private void clearButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.dataReport.Clear();
            this.reloadDataReport();
        }

        private async void addFitbitDataButton_OnClick(object sender, RoutedEventArgs e)
        {
            var date = Convert.ToDateTime(this.dateTextBox.Text);
            var caloriesBurned = Convert.ToInt32(this.caloriesburnedTextBox.Text);
            var steps = Convert.ToInt32(this.stepsTextBox1.Text);
            var distance = Convert.ToDouble(this.distanceTextBox.Text);
            var floors = Convert.ToInt32(this.floorsTextBox.Text);
            var activityCalories = Convert.ToInt32(this.activitycaloriesTextBox.Text);
            var fitbitData = new FitbitData(date, caloriesBurned, steps, distance, floors, activityCalories);

            if (!this.dataLibrary.Any())
            {
                this.dataLibrary.Add(fitbitData);
                this.reloadDataReport();
            }
            else
            {
                foreach (var data in this.dataLibrary)
                {
                    if (fitbitData.Date.Equals(data.Date))
                    {
                        var addDialog = new ContentDialog {
                            Title = "Keep or Replace",
                            Content = "A duplicate day was found. Would you like to keep or replace existing day?",
                            PrimaryButtonText = "Keep",
                            SecondaryButtonText = "Replace"
                        };
                        var addResult = await addDialog.ShowAsync();
                        switch (addResult)
                        {
                            case ContentDialogResult.Primary:
                                //TODO Add keep functionality
                                break;
                            case ContentDialogResult.Secondary:
                                //TODO Add replace functionality
                                break;
                        }
                    }
                }
                this.dataLibrary.Add(fitbitData);
                this.reloadDataReport();
            }
        }

        private async void saveButton_OnClick(object sender, RoutedEventArgs e)
        {
            var builder = new StringBuilder();
            var savePicker = new FileSavePicker {
                SuggestedStartLocation = PickerLocationId.DocumentsLibrary
            };
            savePicker.FileTypeChoices.Add("CSV", new List<string> {".csv"});
            savePicker.SuggestedFileName = "New Document";
            var file = await savePicker.PickSaveFileAsync();
            builder.AppendLine("Date,Calories Burned,Steps,Distance,Floors,Activity Calories");
            var allData = this.dataReport.GetFitbitDataInReport();
            await FileIO.WriteTextAsync(file, allData);
            //TODO Add functionality to notify on file save completion
        }

        #endregion
    }
}