﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace FitbitAnalysis.Model
{
    /// <summary>
    ///     A class representing a collection of all fitbit analysis objects.
    /// </summary>
    public class FitbitDataLibrary : IList<FitbitData>
    {
        #region Properties

        /// <summary>
        ///     Gets the datata list.
        /// </summary>
        /// <value>
        ///     The datata list.
        /// </value>
        public List<FitbitData> DataList { get; set; }

        /// <summary>
        ///     Gets the count.
        /// </summary>
        /// <value>
        ///     The count.
        /// </value>
        public int Count => this.DataList.Count;

        /// <summary>
        ///     Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public bool IsReadOnly => false;

        /// <summary>
        ///     Gets or sets the <see cref="FitbitData" /> at the specified index.
        /// </summary>
        /// <value>
        ///     The <see cref="FitbitData" />.
        /// </value>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public FitbitData this[int index]
        {
            get => this.DataList[index];
            set => this.DataList[index] = value;
        }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="FitbitDataLibrary" /> class.
        /// </summary>
        public FitbitDataLibrary()
        {
            this.DataList = new List<FitbitData>();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" /> to an
        ///     <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">
        ///     The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied
        ///     from <see cref="T:System.Collections.Generic.ICollection`1" />. The <see cref="T:System.Array" /> must have
        ///     zero-based indexing.
        /// </param>
        /// <param name="arrayIndex">The zero-based index in <paramref name="array" /> at which copying begins.</param>
        public void CopyTo(FitbitData[] array, int arrayIndex)
        {
            this.DataList.CopyTo(array, arrayIndex);
        }

        /// <summary>
        ///     Removes the first occurrence of a specific object from the
        ///     <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        ///     true if <paramref name="item" /> was successfully removed from the
        ///     <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if
        ///     <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool Remove(FitbitData item)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Adds the specified fitbit.
        /// </summary>
        /// <param name="fitbit">The fitbit.</param>
        public void Add(FitbitData fitbit)
        {
            if (fitbit == null)
            {
                throw new ArgumentException("Fitbit object cannot be null");
            }

            this.DataList.Add(fitbit);
        }

        /// <summary>
        ///     Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            this.DataList.Clear();
        }

        /// <summary>
        ///     Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        ///     true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />;
        ///     otherwise, false.
        /// </returns>
        public bool Contains(FitbitData item)
        {
            return this.DataList.Contains(item);
        }

        /// <summary>
        ///     Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        ///     An enumerator that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<FitbitData> GetEnumerator()
        {
            return this.DataList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) this.DataList).GetEnumerator();
        }

        /// <summary>
        ///     Determines the index of a specific item in the <see cref="T:System.Collections.Generic.IList`1" />.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.IList`1" />.</param>
        /// <returns>
        ///     The index of <paramref name="item" /> if found in the list; otherwise, -1.
        /// </returns>
        public int IndexOf(FitbitData item)
        {
            return this.DataList.IndexOf(item);
        }

        /// <summary>
        ///     Inserts an item to the <see cref="T:System.Collections.Generic.IList`1" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="item" /> should be inserted.</param>
        /// <param name="item">The object to insert into the <see cref="T:System.Collections.Generic.IList`1" />.</param>
        public void Insert(int index, FitbitData item)
        {
            this.DataList.Insert(index, item);
        }

        /// <summary>
        ///     Removes the <see cref="T:System.Collections.Generic.IList`1" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            this.DataList.RemoveAt(index);
        }

        /// <summary>
        ///     Determines whether this instance is empty.
        /// </summary>
        /// <returns>
        ///     <c>true</c> if this instance is empty; otherwise, <c>false</c>.
        /// </returns>
        public bool IsEmpty()
        {
            return this.DataList.Any();
        }

        /// <summary>
        ///     Calculates the average steps.
        /// </summary>
        /// <returns></returns>
        public double CalculateAverageSteps()
        {
            var average = this.DataList.Average(data => data.Steps);
            return average;
        }

        /// <summary>
        ///     Retrieves all data with steps between.
        /// </summary>
        /// <param name="firstvalue">The firstvalue.</param>
        /// <param name="secondvalue">The secondvalue.</param>
        /// <returns></returns>
        public int RetrieveAllDataWithStepsBetween(int firstvalue, int secondvalue)
        {
            if (firstvalue < 0 || secondvalue < 0)
            {
                throw new ArgumentException("Input values should not be less than 0");
            }

            var steps = this.DataList.Count(data => data.Steps > firstvalue && data.Steps < secondvalue);
            return steps;
        }

        /// <summary>
        ///     Retrieves all data with steps greater than.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public int RetrieveAllDataWithStepsGreaterThan(int value)
        {
            if (value < 0)
            {
                throw new ArgumentException("Input values should not be less than 0");
            }

            var steps = this.DataList.Count(data => data.Steps > value);
            return steps;
        }

        #endregion
    }
}