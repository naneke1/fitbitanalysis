﻿using System;
using System.Threading.Tasks;
using Windows.Storage;
using FitbitAnalysis.Model;

namespace FitbitAnalysis.Processor
{
    /// <summary>
    /// 
    /// </summary>
    public class DataProcessor
    {
        /// <summary>
        /// Gets or sets the library.
        /// </summary>
        /// <value>
        /// The library.
        /// </value>
        public FitbitDataLibrary Library { get; set; }

        /// <summary>
        /// Processes the specified file.
        /// </summary>
        /// <param name="file">The file.</param>
        public async Task Process(StorageFile file)
        {
            var lines = await FileIO.ReadLinesAsync(file);

            lines.RemoveAt(0);

            foreach (var aline in lines)
            {
                var delimeter = new[] { ',' };
                var splitLine = aline.Split(delimeter);

                var date = Convert.ToDateTime(splitLine[0]);
                var caloriesBurned = Convert.ToInt32(splitLine[1]);
                var steps = Convert.ToInt32(splitLine[2]);
                var distance = Convert.ToDouble(splitLine[3]);
                var floors = Convert.ToInt32(splitLine[4]);
                var activityCalories = Convert.ToInt32(splitLine[5]);

                var fitbit = new FitbitData
                {
                    Date = date,
                    CaloriesBurned = caloriesBurned,
                    Steps = steps,
                    Distance = distance,
                    Floors = floors,
                    ActivityCalories = activityCalories
                };

                this.Library.Add(fitbit);
            }
        }
    }
}
