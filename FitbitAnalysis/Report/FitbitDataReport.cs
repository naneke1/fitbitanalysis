﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using FitbitAnalysis.Model;

namespace FitbitAnalysis.Report
{
    /// <summary>
    ///     Gives a report of all fitbit data.
    /// </summary>
    public class FitbitDataReport
    {
        #region Data members

        private const int DaysInYear = 365;
        private const int DaysInLeapYear = 366;

        private readonly FitbitDataLibrary fitbitDataLibrary;

        private readonly StringBuilder stringBuilder;

        private List<FitbitData> dataCollection;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the threshold.
        /// </summary>
        /// <value>
        ///     The threshold.
        /// </value>
        public int Threshold { get; set; }

        /// <summary>
        ///     Gets or sets the steps.
        /// </summary>
        /// <value>
        ///     The steps.
        /// </value>
        public int Steps { get; set; }

        /// <summary>
        ///     Gets or sets the category.
        /// </summary>
        /// <value>
        ///     The category.
        /// </value>
        public int Category { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="FitbitDataReport" /> class.
        /// </summary>
        public FitbitDataReport(FitbitDataLibrary library)
        {
            this.fitbitDataLibrary = library;
            this.stringBuilder = new StringBuilder();
            this.Threshold = 10000;
            this.Steps = 2500;
            this.Category = 6;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Displays a report of fitbit data.
        /// </summary>
        /// <returns></returns>
        public string DisplayFitbitDataReport()
        {
            this.yearlyStepBreakdown();
            return this.stringBuilder.ToString();
        }

        /// <summary>
        ///     Clears this instance.
        /// </summary>
        public void Clear()
        {
            this.fitbitDataLibrary.Clear();
        }

        /// <summary>
        ///     Adds the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        public void Add(FitbitData data)
        {
            this.fitbitDataLibrary.Add(data);
        }

        private void yearlyStepBreakdown()
        {
            var years = new List<int>();

            years.AddRange(this.hasMultipleYearsOfData());

            foreach (var year in years)
            {
                this.dataCollection = new List<FitbitData>();

                this.groupFitbitDataByYear(year);

                this.maxStepsAllYear();

                this.minStepsAllYear();

                this.averageStepsAllYear();

                var stepcount = this.DaysWithStepsMoreThan(this.Threshold);
                this.stringBuilder.AppendLine("# of days with steps more than " +
                                              string.Format("{0:n0}", this.Threshold) + " is " + stepcount);

                this.DaysWithStepsBetween(this.Steps, this.Category);

                this.daysWithNoData(year);

                this.stringBuilder.AppendLine("\n");

                this.monthlyStepBreakdown(year);
            }
        }

        private void groupFitbitDataByYear(int year)
        {
            foreach (var fitbitdata in this.fitbitDataLibrary)
            {
                if (year.Equals(fitbitdata.Date.Year))
                {
                    this.dataCollection.Add(fitbitdata);
                }
            }
        }

        private void monthlyStepBreakdown(int year)
        {
            IDictionary<Months, List<FitbitData>> months = new Dictionary<Months, List<FitbitData>>();

            var values = Enum.GetValues(typeof(Months)).Cast<Months>();
            foreach (var month in values)
            {
                months.Add(month, new List<FitbitData>());
            }

            foreach (var month in months)
            {
                this.addDataToMonth(month);

                if (!month.Value.Any())
                {
                    this.displayOutputForMonthWithoutData(year, month);
                }
                else
                {
                    this.displayOutputForMonthWithData(year, month);
                }
            }
        }

        private void displayOutputForMonthWithData(int year, KeyValuePair<Months, List<FitbitData>> month)
        {
            var index = (int) month.Key;
            var monthName = new DateTimeFormatInfo().GetMonthName(index);
            this.stringBuilder.AppendLine(monthName + " " + year + " (" + month.Value.Count + " days of data)");

            var maxSteps = month.Value.Max(fitbitData => fitbitData.Steps);
            this.stringBuilder.AppendLine("Most steps: " + string.Format("{0:n0}", maxSteps));

            var minSteps = month.Value.Min(fitbitData => fitbitData.Steps);
            this.stringBuilder.AppendLine("Fewest steps: " + string.Format("{0:n0}", minSteps));

            var averageSteps = month.Value.Average(fitbitData => fitbitData.Steps);
            this.stringBuilder.AppendLine("Average # of steps: " + string.Format("{0:n}", averageSteps));
            this.stringBuilder.AppendLine("\n");
        }

        private void displayOutputForMonthWithoutData(int year, KeyValuePair<Months, List<FitbitData>> month)
        {
            var index = (int) month.Key;
            var monthName = new DateTimeFormatInfo().GetMonthName(index);
            this.stringBuilder.AppendLine(monthName + " " + year + " (" + month.Value.Count + " days of data)");
            this.stringBuilder.AppendLine("\n");
        }

        private void addDataToMonth(KeyValuePair<Months, List<FitbitData>> month)
        {
            var index = (int) month.Key;

            foreach (var data in this.dataCollection)
            {
                if (index.Equals(data.Date.Month))
                {
                    month.Value.Add(data);
                }
            }
        }

        private void daysWithNoData(int year)
        {
            int datacount;

            if (DateTime.IsLeapYear(year))
            {
                datacount = DaysInLeapYear - this.dataCollection.Count;
            }
            else
            {
                datacount = DaysInYear - this.dataCollection.Count;
            }

            this.stringBuilder.AppendLine("# of days with no data: " + datacount);
        }

        /// <summary>
        ///     Dayses the with steps more than.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public int DaysWithStepsMoreThan(int value)
        {
            this.Threshold = value;
            var stepcount = this.fitbitDataLibrary.Count(data => data.Steps > value);
            return stepcount;
        }

        /// <summary>
        ///     Dayses the with steps between.
        /// </summary>
        /// <param name="steps">The steps.</param>
        /// <param name="category">The category.</param>
        public void DaysWithStepsBetween(int steps, int category)
        {
            this.Steps = steps;
            this.Category = category;
            var lowerbound = 0;
            var upperbound = steps;
            var count = 1;

            while (count != category)
            {
                var daysBetweenLowerAndUpperBound =
                    this.fitbitDataLibrary.Count(data => data.Steps >= lowerbound && data.Steps <= upperbound);
                this.stringBuilder.AppendLine("Days with steps between " + lowerbound + " and " + upperbound + " is " +
                                              daysBetweenLowerAndUpperBound);
                lowerbound = lowerbound + steps + 1;
                upperbound = upperbound + steps;
                count++;
            }
            var daysGreaterThanUpperBound = this.fitbitDataLibrary.Count(data => data.Steps > upperbound);
            this.stringBuilder.AppendLine("Days greater than " + (upperbound - steps) + " is " +
                                          daysGreaterThanUpperBound);
        }

        /// <summary>
        ///     Gets the fitbit data in report.
        /// </summary>
        /// <returns></returns>
        public string GetFitbitDataInReport()
        {
            var builder = new StringBuilder();
            foreach (var data in this.fitbitDataLibrary)
            {
                builder.AppendLine(data.Date + "," + data.CaloriesBurned + "," + data.Steps + "," +
                                   data.Distance + "," + data.Floors + "," + data.ActivityCalories);
            }
            return builder.ToString();
        }

        private void averageStepsAllYear()
        {
            var averageSteps = this.dataCollection.Average(fitbitData => fitbitData.Steps);
            this.stringBuilder.AppendLine("Average # of steps taken all year: " + string.Format("{0:n}", averageSteps));
        }

        private void minStepsAllYear()
        {
            var minSteps = this.dataCollection.Min(fitbitData => fitbitData.Steps);
            var date = this.dataCollection.Find(data => data.Steps == minSteps).Date;
            this.stringBuilder.AppendLine("Fewest steps taken all year: " + string.Format("{0:n0}", minSteps) +
                                          " and occured on " + date);
        }

        private void maxStepsAllYear()
        {
            var maxSteps = this.dataCollection.Max(fitbitData => fitbitData.Steps);
            var date = this.dataCollection.Find(data => data.Steps == maxSteps).Date;
            this.stringBuilder.AppendLine("Most steps taken all year: " + string.Format("{0:n0}", maxSteps) +
                                          " and occured on " + date);
        }

        private List<int> hasMultipleYearsOfData()
        {
            var years = new List<int>();

            foreach (var data in this.fitbitDataLibrary)
            {
                if (!years.Contains(data.Date.Year))
                {
                    years.Add(data.Date.Year);
                }
            }
            return years;
        }

        private enum Months
        {
            January = 1,
            February,
            March,
            April,
            May,
            June,
            July,
            August,
            September,
            October,
            November,
            December
        }

        #endregion
    }
}