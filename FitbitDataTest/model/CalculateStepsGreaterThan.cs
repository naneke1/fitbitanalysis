﻿using System;
using FitbitAnalysis.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FitbitDataTest.model
{
    [TestClass]
    public class CalculateStepsGreaterThan
    {
        [TestMethod]
        public void TestCalculateAverage()
        {
            FitbitDataLibrary dataLibrary = new FitbitDataLibrary();

            dataLibrary.Add(new FitbitData(new DateTime(1 / 1 / 2016), 1710, 7006, 3.18, 30, 558));
            dataLibrary.Add(new FitbitData(new DateTime(1 / 2 / 2016), 1600, 5478, 2.53, 30, 446));
            dataLibrary.Add(new FitbitData(new DateTime(2 / 3 / 2016), 954, 5756, 2.48, 30, 540));
            dataLibrary.Add(new FitbitData(new DateTime(7 / 11 / 2017), 340, 3756, 1.76, 30, 120));

            Assert.Equals(1, dataLibrary.RetrieveAllDataWithStepsGreaterThan(5000));
        }
    }
}
